# 2. State Of The Art

## 2.1. Key theoretical concepts, approaches and methodologies

This section provides an insight into the concepts, approaches and methodologies used by the DBA and DevOps communities to implement a CI/CD development model to Relational Databases, particularly SQL Server. The next section "2.2. Existing Technologies" covers tools and technologies that apply these concepts and implement these approaches and methodologies.

As stated in Chapter 1 - "Introduction", there are three main concepts to understand in order to apply CI/CD to databases (review Figure 1.1):

 1. Version Control Systems;

 2. Continuous Integration (CI), Continuous Delivery (CD), and the tools that implement them;

 3. Database Lifecycle Management (DLM), and the tools that implement it.

This section will go through all three from a mostly theoretical standpoint, avoiding mentions of specific tools or technologies (which will be covered in section 2.2).

### 2.1.0. Version Control Systems: Centralized vs Distributed

 *"Version control systems are a category of software tools that help a software team manage changes to source code over time. Version control software keeps track of every modification to the code in a special kind of database. If a mistake is made, developers can turn back the clock and compare earlier versions of the code to help fix the mistake while minimizing disruption to all team members."* - Atlassian's definition of Version Control [26].

 *"Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. (...) you can do this with nearly any type of file on a computer."* - Git's take on Version Control [27].


 Version Control is an essential component of modern software development. Keeping tracks of all changes (and associated meta-data, such as authors, times, context, etc.) to all files involved in software development is crucial to allow reverting changes, promote collaboration and accountability amongst software development team members, and ensure traceability and auditability. In addition, Version Control is indispensable for CI/CD processes.

 Regarding how Version Control Systems implement Version Control, there are two major approaches: Centralized and Distributed.

#### 2.1.0.1 Centralized Version Control Systems (CVCS)

*"[Centralized Version Control] systems (such as CVS, Subversion, and Perforce) have a single server that contains all the versioned files, and a number of clients that check out files from that central place. For many years, this has been the standard for version control."* - Git's take on Centralized Version Control Systems [27].

Figure 2.-2 shows a visual representation of a CVCS:

![Centralized Version Control](SOTA_Centralized_VCS.png)

Figure 2.-2 - A generic CVCS

In CVCS, the traditional Client-Server model of distributed computing is employed, where a central Server contains ALL files, and each Client (i.e. a software developer) checks out specific files to be worked on.

CVCS supports the Centralized Workflow [28, 29]:

 1. A developer pulls the latest version of the file(s);

 2. The developer makes changes to the local copy;

 3. The developer commits those changes and pushes them, resolving any conflicts that might be flagged;


#### 2.1.0.2 Distributed Version Control Systems (DVCS)

*"In a DVCS (such as Git, Mercurial, Bazaar or Darcs), clients don’t just check out the latest snapshot of the files; rather, they fully mirror the repository, including its full history. Thus, if any server dies, and these systems were collaborating via that server, any of the client repositories can be copied back up to the server to restore it. Every clone is really a full backup of all the data."* - Git's take on Distributed Version Control Systems [27].

In DVCS, the Peer-to-Peer model is employed, where each Peer contains a full copy of the repository (i.e. both ALL files AND the entire history of commits and changes).

Figure 2.-1 shows a visual representation of a CVCS:

![Decentralized Version Control](SOTA_Decentralized_VCS.png)

Figure 2.-1 - A generic CVCS

DVCS not only support the Centralized Workflow but, as the name implies, a number of different Decentralized Workflows, the most prominent of which are the "Integration-Manager" (Figure 2.-1.1) and the "Dictator-Lieutenants" (Figure 2.-1.2) [29].

![Integration Manager workflow](SOTA_Integration-Manager.png)

Figure 2.-1.1 - the Integration Manager distributed workflow

In the Integration-Manager workflow[29]:

  1. The project maintainer pushes to their public/"blessed" repository.

  2. A contributor clones that repository into his/her "developer private" repository and makes changes.

  3. The contributor pushes to their own public copy ("developer public").

  4. The contributor sends the maintainer an email asking them to pull changes.

  5. The maintainer adds the contributor’s repository as a remote and merges locally ("integration manager" repository).

  6. The maintainer pushes merged changes to the main/"blessed" repository.

This workflow is very common in small to medium-large teams that need to collaborate in a fast, but controlled manner.

![Dictator-Lieutenants workflow](SOTA_Dictator_Lieutenant.png)

Figure 2.-1.2 - the Dictator-Lieutenants distributed workflow

In the Dictator-Lieutenants workflow[29]:

1. Regular developers work on their topic branch ("developer public") and rebase their work on top of master. The master branch is that of the reference/"blessed" repository to which the dictator pushes.

2. Lieutenants merge the developers' topic branches into their master branch.

3. The dictator merges the lieutenants' master branches into the dictator’s master branch.

4. Finally, the dictator pushes that master branch to the reference/"blessed" repository so the other developers can rebase on it.

Although not as common as Integration-Manager, this workflow is useful in very big projects where the main project manager ("dictator") needs to delegate work to intermediary managers/team leaders ("lieutenants").


#### 2.1.0.2 Centralized vs Decentralized - a comparison

 Here are the main benefits and drawbacks of each kind of VCS.

 CVCS [30]:

  + **low storage requirements in client machines**, since files can be checked out individually, and the repository history does not need to be copied over;

  + **good with binary files**. File-locking mechanisms prevent conflicts. Additionally, these files do not take up space in the client machines until they are explicitly checked out;

  + **file-level permissions**. A CVCS like SVN allows administrators to configure each contributor's permission level, for each individual file or directory if necessary. A DVCS like Git, for example, does not.

  - **inflexible workflow**. Compared to DVCS, which has multiple workflows for a team to choose from, a CVCS only allows the Centralized workflow;

  - **lower userbase**. Although this is an external factor, it still bears mentioning that DVCS have a much larger userbase (as shown in the "Existing Technologies" further ahead). New hires would probably be more comfortable with DVCS from the get-go than CVCS.

DVCS:

  + **very fast on all actions, excluding Pull/Push**. *"Performing actions other than pushing and pulling changesets is extremely fast because the tool only needs to access the hard drive, not a remote server."* [28]

  + **more freedom to work offline**. Developers can work on a feature, breaking down the changes into multiple commits, and then Pushing them in one go;

  + **lightweight branching**. Creating branches is usually faster and simpler than in CVCS;

  - **larger storage requirements on client machines**, which can be problematic for repositories of significant size and history. Keeping local copies of massive repositories can take up a lot of time and disk space. This issue is exacerbated by the next one;

  - **inefficient for large/numerous binary files**. Although it is good practice to keep binary files out of source control as much as possible, sometimes it is a "necessary evil". Keeping multiple versions of these files as time goes by, and in every single developer's machine, can become extremely inefficient in terms of disk space.

### 2.1.1. Continuous Integration and Continuous Delivery - CI/CD basics

 Continuous Integration and Continuous Delivery are techniques traditionally applied to software application development. Before delving into CI/CD as it applies to Relational Databases, it is imperative to first understand it as it relates to software applications.

#### 2.1.1.1 Continuous Integration

 *"Continuous Integration is a software development practice where members of a team integrate their work frequently, usually each person integrates at least daily - leading to multiple integrations per day. Each integration is verified by an automated build (including test) to detect integration errors as quickly as possible. Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly."* [8] - Martin Fowler, one of the twelve signees of the Agile Manifesto [9].

 Figure 2.0, adapted from [10], presents an overview of a typical **Continuous Integration pipeline**:

 ![Continuous Integration](SOTA_CI.png)

 Figure 2.0 - Continuous Integration overview

 1. Developers check code into a version control repository. **There is no CI without Version Control**;
 2. The VCS notifies the CI server that a commit has occurred (OR the CI server polls the repository periodically looking for commits);
 3. The CI server triggers a Build process, assigning it to a pool of Build Agents. For simple processes a single available Agent can be sufficient, but it is standard practice to use pools to:

  - perform multi-stage Builds in the case of more complex applications where those stages can happen concurrently;

  - auto-assign Agents to Builds in cases where a business might be building multiple apps simultaneously. Especially useful when adopting the micro-services approach of having multiple, simpler apps being part of a greater solution instead of a single, monolithic one;
 4. The code containing the latest commit is checked out of / pulled from the repository into a local workspace on the build server;
 5. The code is built and then tested. Typically only Unit Tests are performed, in order to keep the Build process as fast as possible. **There is no CI without Unit Tests**, as there is no automated way to detect simple integration problems early. Further tests of a more complex nature are discussed in the Continuous Delivery section;
 6. Build results are reported back to the CI server: did the code compile and did it pass Unit Tests, along with any important files that need to be retained. If the Build was successful, the Build Agents create one or more Build Artifacts, gather them into a Package, and upload it to the CI Server. "An artifact is a deployable component of your application" [11]. In other words, a single Build Artifact refers to a runnable or installable version of a developed application, in the form of a set of binary files for compiled languages (e.g. a JAR for Java apps, .pdb/.dll/.exe files for a C# console app) or script files for interpreted languages (e.g. .js scripts for Node, .py for Python). When a CI process only produces one Build Artifact, "Artifact" and "Package" are sometimes used interchangeably;
 7. The CI server produces the overall outputs of the CI pipeline. The first output is the final result of the build, and notifying the Developer Team (and other stakeholders). The second output is the latest available Build Artifact, Published by the CI server.

 Given that a CI pipeline produces a Build Artifact / Package, it is also colloquially referred to as a **Build Pipeline**.

#### 2.1.1.2 Continuous Delivery (or Deployment?)

 After setting up a Continuous Integration pipeline, the only result so far is a Build Artifact that exists in some CI server somewhere accessible by the software company that produced it, but **is not actually being used by any of its users**. The environment where an application can be accessed and used by customers and clients is known as a **Production** or **Live** environment. It can be a set of live web servers for web apps, customers' desktops for desktop apps, or smartphones for mobile apps.

 *"Continuous Delivery is the ability to get changes of all types—including new features, configuration changes, bug fixes and experiments—into production, or into the hands of users, safely and quickly in a sustainable way."* [12] - Jez Humble. Author of "Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation", 2010, teacher at UC Berkley.

 **Continuous Delivery** (CD) is therefore the gap between Continuous Integration and getting software into users' hands. Continuous Delivery takes the Build Artifact produced by the Continuous Integration pipeline as its primary input, and subjects it to a **Delivery Pipeline** that includes several different intermediary **testing environments** with increasingly complex tests, to ensure that the Artifact is bug-free and working according to requirements. This process culminates in the deployment of that Artifact into a **Production** environment. [12] also asserts that *"the input to the [Delivery] pipeline is a particular revision in version control. Every change creates a build that will, rather like some mythical hero, pass through a sequence of tests (...). This process (...) is begun with every commit to the version control system"*. However, according to the CI definition espoused in section 2.1.1.1, this is not exactly so. Not every commit will generate a Build Artifact - only those that pass unit tests will. Otherwise, the Delivery pipeline would be kept busy testing builds that did not even pass basic unit tests.

 The number of environments depends on the kinds of tests that will be performed. Here is a brief overview of a generic CD pipeline with 4 different Environments - Testing, User-Acceptance Testing (UAT), Staging, and Production:

 ![CD](SOTA_CD.png)

 Figure 2.1 - A simple, generic CD pipeline

 1. The CI server publishes a new Build Artifact (BA), making it available to the CD pipeline;
 2. The CD server listens in on any newly available BAs. The CI and CD servers might communicate directly, or they can be decoupled by having the BA be published to a feed that both servers connect to (the CI acting as a publisher, and the CD as a subscriber);
 3. Every time a new BA is available, the CD server begins a deployment process to the Test Environment, by triggering/alerting its Agents. An **Environment** consists of one or more machines where specific tests of the BA will take place. There is a **Deployment Agent** installed on each of these machines. An Agent is responsible for fetching the BA to its local machine, run tests, and notify the CD server of results;
 4. The Test Agents fetch the BA, install it and run the designated tests;
 5. The Test Agents notify the CD server of test results - PASS or FAIL;
 6. If the BA passed all the tests in the Test Environment, the CD server triggers a deployment to the next Environment: User-Acceptance Testing (UAT). **If the BA did not pass all the tests, the BA's lifecycle ends, as it will progress no further along the pipeline.** A new BA will have to be generated by the CI pipeline (i.e. developers and other stakeholders will have to fix whatever caused the failure, and commit it to trigger a new CI build). In this context, UAT is understood to contain tests performed by human users, that cannot be automated;
 7. and 8. The same as 4. and 5., but for the UAT Environment instead of the Test one;
 9. The same as 6., but for the Staging Environemnt instead of UAT. In this context, Staging refers to an Environment that mirrors Production as closely as possible;
 10. and 11. same as 7. and 8., but for Staging instead of UAT;
 12. If the BA passed Staging, and therefore all previous Environments, it is available to be deployed to Production. At this stage, there is a crucial decision to be made: should every available BA be deployed to Production as soon and as frequently as possible, with an automatic trigger? If so, that is a case of **Continuous Deployment**. If, instead, deployments to Production should not occur automatically and at every available opportunity, but instead be regulated by a final human authorization (i.e. whatever Stakeholders might have the authority to do so), then it is a case of **Continuous Delivery**, as illustrated in the figure;
 13. The authorized Stakeholders trigger the deployment to Production, through the CD server, which notifies the Production Agents;
 14. The Production Agents install the BA in the Environment, and the BA is now **live**;
 15. The Production Agents notify the CD server if the deployment was successful or not.

As stated above, this is just an example of possible environments. There is no industry-standard for specific ones, and their designations can sometimes differ from one company to the next (e.g. what is thought of as "UAT" in one place might be called "Staging" in another). The important concept to grasp is that there should be intermediate environments before a Build Artifact is deployed straight to Production, and that tests get more complex and time-consuming from one environment to the next. Additionally, it is common practice to split up the Testing Environment according to specific kinds of tests (e.g. an "Integration Tests" Environment and a "Regression Tests" Environment) and to parallelize test execution (i.e. the same BA can be deployed simultaneously to the Integration and Regression environments, and if it fails in any of these sub-environments, progression to the next Environment is stopped).


### 2.1.2. Why applying CI/CD to Databases is not as trivial as applying it to Applications


Applications are stateless. Figure 2.2 illustrates that deploying one to a new Environment (e.g. Testing, UAT, Staging, Production) is as "simple" as downloading the Build Artifact to that Environment's machines, deleting the old version of that Build Artifact, and moving the new one into place (e.g. copying the BA to a specific, designated folder on the Environment's machines, or installing it locally):

![Deploying an App](SOTA_Generic_App_Deployment.png)

Figure 2.2 - Deploying a Build Artifact to a given Environment. It is somewhat trivial and straightforward to simply replace the old BA with the new one


What would happen if the same approach were to be used for Databases? Figure 2.3. shows the appropriate approach:

![Deploying a Database](SOTA_DB_Deployment.png)

Figure 2.3 - Deploying a Database to a given Environment. It is not the same as deploying an Application Build Artifact

Unlike applications, databases cannot simply be deleted and replaced by a new version, because **Databases are not stateless.** Their whole purpose is to preserve state (i.e. data), before and after each new deployment. Dropping a database and creating a new one from scratch would result in catastrophic data loss to the business.

One possible strategy would be to backup the old Database, delete it, create the new versioned one, and then restore the backed up data. However, backing up and restoring an entire database can take hours. Considering that several changes can be made to multiple databases in a day, this would make it impractical to apply the core CI/CD and DevOps principle to "Build and Deploy often", rendering this option unfeasible.

Due to these particular needs, there are two major approaches when it comes to database development and approaches: the State-based approach and the Migrations approach. Both are SQL script-based strategies with their own sets of strengths and weaknesses. Regardless of the chosen approach, the CI and CD pipelines for database development will be very similar.

Figure 2.4 shows what a CI pipeline for database development should look like:

![A CI pipeline, but for databases](SOTA_CI_DB.png)

Figure 2.4 - A CI pipeline similar to the one in Figure 2.0, adapted for database development

The process is similar to the one described in Figure 2.0, with the following key differences:

1. Developers commit **SQL scripts** to version control. These scripts are either written according to the State-based approach OR the Migrations-based approach;
5. The Build Agents apply/run SQL scripts on a Unit Tests database. These unit tests are written and run with an SQL-specific testing framework, like for example "tSQLt" [15];
6. The Build Artifact consists of all the SQL scripts in version control;
8. The BA is published, with all version-controlled scripts within. It will be the CD pipeline's responsibility to determine **how to use** these SQL scripts, depending on if the State or Migrations approach is used.

Figure 2.5 shows what a CD pipeline should look like:

![A CD pipeline, but for databases](SOTA_CD_DB.png)

Figure 2.5 - A CD pipeline similar to the one in Figure 2.1, adapted for database development

### 2.1.3. Database Lifecycle Management, or the development and deployment approaches: State vs Migrations

In order to apply CI/CD to databases, a major choice must be made that will impact the entire process, from development to deployment: what is the source of truth that defines the database? There are two major alternative answers: State and Migrations [1, 2]:

  1. **State** - The source of truth is a set of SQL scripts in source control describing how the database **should be**. Each script describes a different, individual database object as if it were to be created from scratch (e.g. Tables, Indexes, Views, Stored Procedures, Triggers, etc);

  2. **Migrations** - The source of truth is a pair of sets of SQL scripts in source control: the first set of scripts describes an initial state for the database, known as the **baseline** (these scripts can be declarative and have a one-to-one match to each individual database object, just like in the State approach), and the other set describes how the database **should change**. This second set of scripts is known as the **migrations**. Each of these scripts **migrate** the database from one state to the other.

These approaches are related to the broader Database Lifecycle Management (DLM) concept, as defined by Microsoft [22], RedGate [23] and Oracle [24]. Although DLM is an extensive set of concepts and policies that are not fully covered by the scope of this report, the term "DLM" will henceforth be used in a synecdochical manner to refer only to the "State vs Migrations" dilemma, and the set of tools that implement these two different approaches.

#### 2.1.3.1. The State Approach

In the State approach, also known as the Model or Declarative Approach [1], every single database object (e.g. Tables, Indexes, Views, Stored Procedures, Triggers, etc) is represented by a declarative SQL script, with mostly SQL "CREATE" statements [1, 2], like the one in Figure 2.6:

![A simple CREATE TABLE statement](SOTA_CREATE_Person_Before.png)

Figure 2.6. A simple CREATE TABLE statement

The source control folder and file structure for State-based solutions typically looks like the content of Figure 2.7:

![State-based folder structure](SOTA_State_Folders.png)

Figure 2.7. Typical folder structure for the State-based approach. Grouping files by database schemas (e.g. "dbo", "Person", "Purchasing") and then by database object type (e.g. the "Functions", "Stored Procedures" and "Tables" folders in the "dbo" schema folder) is a common technique.

When a database developer needs to make changes to a database object, the corresponding SQL script must be modified.

Example: suppose that a "Person" table exists, with the "FullName" and "Phone" columns, and represented by a "Person.sql" script as shown by Figure 2.8:

![Person Table](SOTA_CREATE_Person_Before.png)

Figure 2.8 - Initial Person.sql script

Suppose that a database developer needs to add an "Email" column to that table, which can be done by simply typing it in the same file, as shown on Figure 2.9:

![Person Table](SOTA_CREATE_Person_After.png)

Figure 2.9 - Person.sql script after adding a column

When this change is committed to source control, it will trigger a CI/CD process, as shown in Figure 2.10:

![CI CD Pipeline for the State Approach](SOTA_CI_CD_State_DB.png)

Figure 2.10 - A generic CI/CD pipeline similar to the ones typically used for Applications, but adjusted for Databases and in accordance with the State development approach

Here is a brief summary:
 . a Developer changes the "Person.sql" script to add an "Email" column to the "Person" table, and commits the change to source control;
 . the Continuous Integration server makes use of an automated SQL-comparison tool to compare the scripts in source control to a database located in a Dev/Build Environment and generate a SQL script to bring the database to the same state described by all the source-controlled SQL scripts. This generated script is **not** in source control, and not committed to it at any point;
 . the server attempts to apply the generated script to the Dev database. If this succeeds (analogous to a "Build Successful" in the case of Applications), the source controlled SQL scripts are packaged as a Build Artifact, and passed on to the Continuous Delivery pipeline, where the SQL-comparison tool generates a new script for each Environment, comparing the scripts in the BA to each database's current state.

The State approach generally presents the following strong suits [1-4]:

 . **similar approach to application source code**. SQL code describes database objects in a way that closely resembles how app source code describes classes or methods, which can make life easier for app developers transitioning to an SQL-developer role;

 . **excellent for programmable objects**, such as Triggers, Views, Functions and Stored Procedures. The fact that these objects are stateless means there is no risk in recreating them from scratch for each deployment (usually through "CREATE OR REPLACE" statements)

 . **easy to gauge the state of a database** through the source controlled scripts;

 . **good at spotting and handling conflicts**. This becomes critical as the developer team increases in size.

 . **reliable testing**. Tests always target a database's ideal state;

 . **developers are free from writing migration/change scripts**;


 The State approach also presents the following issues, however:

  . **context unawareness** [1]. Tools might generate scripts that "break" a database in the case of non-trivial changes, such as column merging, table renaming, and other refactoring operations. Tools are generally good at capturing the current state of a database, but they lack context in deciding how to drop columns or tables;

  . **inconsistent deployments** when databases in different Environments are in different states, due to a Build Artifact passing one Environment, but not the subsequent ones.

Context unawareness is explained further in the cited reference, but the issue of inconsistent deployments is deserving of additional clarification. Consider Figure 2.11:

![A generic set of Environment databases](SOTA_State_Deploy_Diffs_Before.png)

Figure 2.11 - A generic set of Environment databases

In this simple scenario, the source controlled SQL files "Person", "Customer" and "Price" represent a "Sales" database. Each of the "Testing", "UAT" and "Staging" Environments have their own version of the Sales DB for testing purposes. Alice created the base SQL scripts describing a hypothetical database in a given "State A" in Commit 1, which passed all Environments, putting them all in State A. Bob modified the scripts in Commit 2 for a User Story he is working on. Commit 2 passed Testing, putting it in "State B", but failed in UAT, putting it in State B also. Because of the UAT failure, Staging never goes to State B. Alice attempted to fix the issue in Commit 3, but accidentally introduced an error that caused things to fail in the earlier Testing Environment. Testing goes from State B to "State C", UAT never goes to State C (remaining in State B), and Staging remains in State A.

Now consider that Bob makes an additional Commit 4 as a second attempt to fix the underlying issue, which leaves the source control scripts in State X. Assume that this fix works, and Commit 4 passes all Environments. Will it leave every Environment DB in the exact same state with absolute guarantee? How will the changes in Commit 4 actually be deployed to each Environment? Figure 2.12 provides a snapshot:

![Different migration scripts for different Environments](SOTA_State_Deploy_Diffs_After.png)

Figure 2.12 - Different migration scripts for different Environments

When deploying to the initial Testing Environment, a SQL comparison tool analyzes the Test DB, compares its State C to the scripts' State X, and generates a migration file "StateC_to_StateX.sql", which is then run on the Test DB. Testing succeeds, so the deployment process repeats for the UAT Environment, with the SQL comparison tool generating a different migration script adjusted for each Environment's State. So in practice, an automated tool is being relied on to analyze each Environment's database, understand the context of each change that needs to be done in a single instant of time, and understand how to ensure these changes remain consistent as time goes by. Given that Figure 2.12 is only a snapshot of Commit 4's deployment, Figure 2.13 presents the timelines of all the Environments:

![Timelines for all Environments](SOTA_State_Deploy_Time_States.png)

Figure 2.13 - Timelines for all Environments

Notice that between Commit 1 and Commit 4, Testing goes through 3 changes, UAT goes through 2, and Staging goes through 1, and if the goal is for each database to achieve the exact same state, then the underlying assumption is that:

 StateA_to_StateB + StateB_to_StateC + StateC_to_StateX

 <=> StateA_to_StateB + StateB_to_StateX

 <=> StateA_to_StateX


**An automated tool might not be suitable to ensure this equivalence** and that **deployments are consistent and reproducible**, despite its comparison capabilities. Although different tools certainly have their own source code and differentiating aspects, much of techniques employed by these tools can be traced back to the seminal work "Refactoring Databases", by Scott J. Ambler and Pramod J. Sadalage [46]. However, one approach that is theoretically capable of ensuring this is the Migrations one.

#### 2.1.3.2. The Migrations Approach

In the Migrations approach, there are two sets of SQL scripts in source control: the **baseline** and the **migrations**. The baseline can be a single, complex script that creates a database in one go, or it can itself be a set of scripts where each one creates an individual database object, similarly to the State approach. The migrations are **manually written scripts that describe database changes in a sequential manner**. This is in contrast to the State approach, where these scripts would be generated in deploy-time by a comparison tool, for each Environment database. A typical folder structure for the source control repository in the Migrations approach is shown in Figure 2.15:

![Migrations-based folder structure](SOTA_Migration_Folders.png)

Figure 2.15 - Migrations-based folder structure

It is standard practice to number these files with a prefix (e.g. "001_(...)", "002_(...)") so that a **migrations tool** can run them in an appropriate order during deployments. In this example, using RedGate's "SQL Change Automation" suit [5], the tool would first order the scripts by their parent folder, and then by their filename. Most solutions allow different ordering formats to be used for the prefix according to the user's preference - simple increments ("001_AddCustomerIndex.sql") and timestamp-based ("20190516-1703_AddCustomerIndex.sql") are common options. Given that these migrations scripts **describe change instead of state**, their filenames should reflect this accordingly with meaningful verbs/operation names ("Add", "Rename", "Remove", "Merge", "Split", "Create", etc), the affected object type ("Table", "Index", "Column") and the object name [6].

Regarding typical file content, while the **declarative** scripts in the State-based approach usually consist of SQL "CREATE" statements, the **imperative** migration scripts usually consist of SQL "ALTER" statements. Going back to the example introduced in Figure 2.5 in the "State Approach" section, suppose that the original "Person" table must be modified to include a new "Email" column. This would be accomplished with the migration script in Figure 2.16:

![A simple ALTER statement, common in the Migrations approach](SOTA_ALTER_Person.png)

Figure 2.16 - A simple ALTER statement in a hypothetical "20190516-1735_ALTER_TABLE_Person_AddEmail.sql" migration script, common in the Migrations approach

This is the same statement that an SQL-comparison tool would generate in the State-based approach, as shown previously in its respective CI/CD pipeline (Figure 2.8). In the Migrations approach however, this was created by a human developer. Although it would seem feasible to trust an automated tool to produce this script instead of a developer, the risk of error increases as changes become more complex and context-dependent. Therefore, instead of using a **script generation tool** like in the State approach, this approach makes use of a **migration tool**. Figures 2.17 and 2.18 show an example of how this tool would work in the general CI/CD workflow:

![Migrations tool before acting](SOTA_Migrations_Deploy_Diffs_Before.png)

Figure 2.17 - Scenario before the migrations tool is used

Commit 1 - In this scenario, suppose that Bob created a "1.0.0-Baseline.sql" file. The SQL script describes a database in an initial state. The CI server detected that change, published the script to the CD server, which then made sure to apply it to each environment database - Testing, UAT, Staging - in that order. The script was applied successfully and all tests passed in every environment, leaving every database updated up to Commit 1;

Commit 2 - Then, Alice had to add a feature related to a customer's favorite food, so she created the "1.0.1-001-AddFavoriteFood.sql" file, that adds the "FavoriteFood" column to a "Customer" table. She also wanted to create an associated index for the new "Food" column, so she created that in "1.0.1-002-AddCustomerIndex". She named these files in such a way so that the "FavoriteFood" one runs before the "Index" one. Unfortunately, something went wrong: the scripts were run successfully on the Testing database, but a failure occurred in UAT. Testing and UAT are now up to Commit 2, but Staging is still on Commit 1;

Commit 3 - After being notified of the UAT failure, Bob tries to fix it by committing a new migration file, "1.0.1-003-FixCustomerIndex.sql". However, it actually introduced some new error, since this new commit failed in Testing. Testing is now on Commit 3, UAT is on Commit 2, and Staging is still on Commit 1;


Now, Alice believes she has found the right fix for this issue, and is ready to commit the file "1.0.1-004-2nd_fix.sql". Figure 2.18 shows what will happen after she does so:

![Migrations tool after acting](SOTA_Migrations_Deploy_Diffs_After.png)

Figure 2.18 - Scenario after the migrations tool is used

The CD pipeline starts by deploying all the scripts in version control to the Testing database. The SQL migrations tool looks up a **migrations meta-table** in the Testing DB, which keeps track of which migrations have already been applied. The meta-table's latest record is of migration "1.0.1-003", so the tool is able to determine that the remaining migration to apply is "1.0.1-004". Tests pass, so the CD process continues in the same way for the UAT and Staging databases. The migrations tool is able to determine, for each database, which migrations are pending deployment (1.0.1-003 and 1.0.1-004 for UAT, and additionally 1.0.1-002 and 1.0.1-001, which hadn't been applied beforehand), and **runs them in the exact same order, for every database, ensuring the final state is exactly the same**.

Notice that by the end of Commit 4's deployment, every single database has had the exact same five migration scripts run applied to them in the exact same order, although at different points in time. This **ensures reproducible deployments**, even if the databases tend to reach different states as the result of different tests picking up different issues and bugs.

Having covered the way the Migrations approach works, here are its strong suits:

 . **fully controlled deployments** - SQL developers explicitly state how the database must change, leaving no room for uncertainty;

 . **reproducible deployments** - Migrations are sequential and are always applied in the exact same order

 However, the Migrations approach presents the following weak points:

 . **harder to resolve merge conflicts**. If developers work simultaneously on the same database object, conflicts will be much harder to identify and resolve than in the State approach;

 . **hard to quickly grasp the state of a given database object from the SQL scripts**. It might be necessary to look through a dozen SQL scripts to answer the question "What columns does the Person table currently have?". In the State approach, a developer would only need to look at the "Person.sql" script;

 . **inefficient and potentially unreadable for programmable objects**. Views, Triggers, Functions, Stored Procedures, etc., are stateless. Making changes to a Stored Procedure over time in the form of a dozen "REPLACE PROCEDURE" migrations can become unwieldy. To counteract this issue, most tools that implement the Migrations approach offer an "exception" - use the State approach for programmable objects, declaring them in their own single SQL file, making use of "CREATE OR REPLACE" statements (ensuring idempotency);

Given its strengths and weaknesses, the Migrations approach implies the following essential rules that all developers must abide by:

 . **use issue-tracking to avoid working on the same object simultaneously**;

 . **always respect the team's file naming conventions**, to ensure that migrations are always run in the correct order;

 . **keep migrations as atomic and small as possible**. This will make it easier to Integrate and Deploy more often;

 . **do not change a Migration script after it has been committed**. If it is necessary to fix an error that a previous migration script introduced, create a new migration instead;

 . **make Migration scripts idempotent**. In other words, design them to have the same effect on a database regardless of its current state. Checks, guards and verifications in the SQL-code are important techniques to use;

 . **periodically re-baseline the project**. [42].
#### 2.1.3.3. Issues that neither the State nor the Migrations approaches can solve on their own

##### Schema Drift

 *"Schema Drift occurs when a target database deviates from the baseline that was used to originally deploy it."* - [25], from RedGate's documentation

 Suppose that a critical issue is found to be happening in a Production database currently on version 1.2. This issue is causing significant losses to the business so Bob, the lead DBA, has to act quickly. He identifies the cause and decides to run a SQL script manually on the Production DB, **without submitting it to version control and the CI/CD pipeline** to fix things as soon as possible. Figure 2.19 provides an overview:

   ![Schema Drift](Schema_Drift.png)

Figure 2.19 - Schema Drift often happens when trying to fix things in Production

Although the main issue has been solved, the Production database is now in a state that is completely alien to version control (and the rest of the CI/CD pipeline by extent), which is the most common form of Schema Drift. Schema Drift implies the following problems:

 . **untested Production database** - because the changes to Production did not have to go through the CI/CD pipelines, they have not been subjected to any kind of tests;

 . **uncertain deployments** - for the State approach, there could be issues in generating the correct change script during the next deployment to Production. For the Migrations approach, the next deployments will still leave Production in an unknown state.


To prevent Schema Drift: all database changes must go through the CI/CD pipeline. However, no matter how disciplined a team is and despite their best intentions, it is likely that there will be an extreme circumstance where the previous rule will have to be broken, just like the example above demonstrated.

To fix Schema Drift:

 - for the State approach: translate the changes that have been applied to Production into a declarative form and add it to the appropriate object script(s) in source control. The automated comparison tool should then be able to generate appropriate change scripts for each environment database;

 - for the Migrations approach, in a controlled way: when fixing the Production database, run that script as a migration with the migrations tool (i.e. use the agreed-upon naming convention, and use the migrations tool to run it so it counts it as a migration). Then, introduce that script into version control, and everything should work fine. If it was not possible to run the fix script;

 - for the Migrations approach, in an ad hoc way: if it was not possible to run the fixes to Production as a migration script, manually edit the meta-table in Production to include that migration (e.g. if the last record on the table is "20190512_1351-ALTER_TABLE_Person_Add_Email.sql", then INSERT a new record like "[some timestamp]-Fix_Production.sql"), and then commit a new migration to version control with the exact same name "[some timestamp]-Fix_Production.sql" and the fix as its content.

 - Merge conflicts;

 - Lack of discipline;

#### 2.1.3.4. Which approach is better? A preemptive verdict

 Ultimately, neither State nor Migrations are inherently superior. Both have advantages and disadvantages, and as shown in the previous sections and as espoused by several authors [1], some teams might benefit from one and others from the other.

 Having studied the two approaches independently, and having collected feedback from DBA/DevOps community members from companies such as i2S, Natixis and Critical Manufacturing, the author feels that the Migrations approach is the one that will be most benefitial in the long run, provided that all team members are made aware of the necessary effort to communicate and coordinate.

## 2.2. Existing technologies

This section describes the existing technologies necessary for implementing CI/CD. It is divided into three main subsections: **Version Control Systems**, **DLM tools**, and **Continuous Integration and Continuous Delivery tools**.

### 2.2.1 Version Control Systems

To understand what options are on the market, whether commercial or open-source, it can be fruitful to survey the developer community for insight. Therefore, Figure 2.19 shows the results to the question "Which Version Control System do you use?", in the 2018 StackOverflow Developer Survey [16]:

![Version Control survey, by stackoverflow 2018](SOTA_Git_Survey.png)

Figure 2.19 - Version Control usage by Professional Developers, in the 2018 StackOverflow Developer Survey

Git is clearly the dominant market leader, with almost 9 out of 10 professional developers making use of it. Subversion (SVN) and Team Foundation Version Control (TFVC) are the other major alternatives, with Mercurial a distant 4th (although technically the "network shares" and "zip backups" answers were ranked higher, they should not be considered true version control systems as they are too much of an ad hoc solution). Therefore, this section will briefly cover Git, SVN, TFVC and Mercurial. Figure 2.20 shows a summary of what to expect from these four tools:

![Available VCS](VCS_comparison_table.png)

Figure 2.20 - VCS summary

#### 2.2.1.1 Git

 *"Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency."* - from https://git-scm.com/, [17]

 Git is a DVCS, and perhaps the most widely known and used VCS in the world. Its main features are the **staging area**, which allows breaking up changes into smaller units of work before committing them, and the ability to rewrite local repository history through commands and options such as "stash", "commit --amend", and "rebase", among others. It is also possible to rewrite the history of main repositories, through more complex processes. Despite the possible advantages, it is possible to abuse/misuse this ability in ways that result in **lost history**.


#### 2.2.1.2 Mercurial

 *"Mercurial is a free, distributed source control management tool. It efficiently handles projects of any size and offers an easy and intuitive interface."* - from https://www.mercurial-scm.org/, [32]

 Mercurial is the second most widely used DVCS, after Git. Its main selling points are its enforcement of repository history, and a command syntax more familiar to those used to CVCS, and overall easier to learn [33, 34]. Its main disadvantages are the lack of a staging area, and smaller userbase (although popularity does not equal quality, it is undeniable that any business using Git will most likely have new hires who are familiar with Git rather than Mercurial, thus saving training time and costs).

#### 2.2.1.3 SVN (Subversion)

*"Enterprise-class centralized version control for the masses. Subversion is an open source version control system."* - from https://subversion.apache.org/, [35]

Subversion, or SVN for short, is the most widely used CVCS in the world. Despite its ranking on the 2018 StackOverflow Developer Survey (shown above), it has more representation in the enterprise world as opposed to smaller businesses and developer teams. The main reasons for this enterprise persistence is SVN's history - initially available around 2002-2003 in beta form [36] and in version 1.0 in 2004, while Git only gained wider adoption around 2008-2010 when GitHub, a project-hosting service, itself gained popularity. Due to enterprise environments being generally more inert in terms of migrating to new technologies or systems, SVN has persisted as a safe and well known choice for VCS.

SVN's major benefit, in comparison with DVCS such as Git and Mercurial, is the ability to assign different permissions to specific files, directories, and users, which allows teams to work on a "need to know" basis whenever necessary. In DVCS, when faced with a similar permissions issue, the alternative would be to split up a repository into smaller ones according to their purpose and who would be using them.


#### 2.2.1.4 TFVC (Team Foundation Version Control)

*"Team Foundation Version Control (TFVC) is a centralized version control system. Typically, team members have only one version of each file on their dev machines. Historical data is maintained only on the server. Branches are path-based and created on the server."* - from the official TFVC documentation, [38]

Team Foundation Version Control (TFVC) is Microsoft's proprietary CVCS. TFVC is a component of Team Foundation Server (rebranded as "Azure DevOps Services" from 2019 onwards, itself discussed in a later section of this report), a requirements management and project management suite from Microsoft, and a successor to 3C TechLab's currently used "Visual Source Safe 2005". Like most CVCS, its main benefits are granular permissions and handling large binary files.

The most relevant point about TFVC, however, is Microsoft's apparent encouragement of using Git instead:

*"Git is the default version control provider for new projects. You should use Git for version control in your projects unless you have a specific need for centralized version control features in TFVC."* [37]

Although somewhat speculative, it might be that Microsoft plans to phase out TFVC. In addition, while Git, Mercurial and SVN are open standards compatible with multiple CI/CD tools, TFVC is by definition oriented towards Microsoft's Azure DevOps, which could result in vendor-lock.


### 2.2.2 Database Lifecycle Management (DLM) tools

This section covers the most prominent Database Lifecycle Management (DLM) tools on the market. State-based tools are presented first, followed by Migrations-based ones. Figure 2.21 shows an overview:

![DLM tools](DLM_comparison_table.png)

Figure 2.21 - DLM tools overview

All of the mentioned tools were tested by the author, both by themselves and integrated into CI/CD tools (discussed in the next section), save for the ones where a "Free Trial" was not available. Most fields are self-explanatory, except for "IaC-capable". This field indicates if the tool is clearly compatible with the "Infrastructure as Code" paradigm (a great complement to CI/CD, and an important component of the broader DevOps movement). This concept is further explained in the Glossary, but in order to interpret this field, the key point is whether or not a given tool can be integrated into a CI/CD pipeline through command-line scripts (e.g. Powershell/cmd for Windows, bash for Linux).

#### 2.2.2.1 SQL Server Data Tools (SSDT)

*"SQL Server Data Tools (SSDT) transforms database development by introducing a ubiquitous, declarative model that spans all the phases of database development inside Visual Studio. You can use SSDT Transact-SQL design capabilities to build, debug, maintain, and refactor databases. You can work with a database project, or directly with a connected database instance on or off-premise."* - from SSDT's official documentation, [39]


SSDT is a Microsoft DLM suite based on the State approach. It is free for commercial Visual Studio license holders. It uses the concept of DACPACs (Data-Tier Application Package) to encapsulate all database objects into a SQL Server project. SSDT is used as a GUI plug-in for Visual Studio, but the main lower-level tool behind SSDT is SqlPackage [40], which provides a set of commands to package, compare and publish databases, among others. These commands can then be used with CI/CD tools to build the respective pipelines.

Whenever changes need to be deployed from development to other Environments in the CD pipeline, SSDT uses SqlPackage to produce a DACPAC representative of the database's new schema, compare that DACPAC with a target database in each specific Environment (e.g. compare the DACPAC with a "TestDB"), and auto-generate a migration script to upgrade that target database (a process that mirrors and implements the one described in the "State Approach" section of this report).

SSDT's main benefits:

  . Free;

  . All of the benefits and strong suits discussed in the "State Approach" section;

 SSDT's main weaknesses:

  . Keeps track of changes in a "refactorlog.xml" file, which then forms the basis for generating migration scripts. While the tool was being tried out, this file quickly became unwieldy and human-unfriendly after a couple of commits and changes - this log would be indispensable to troubleshoot potential deployment issues created by SSDT, and its unreadability would cause significant time costs.

  . All of the weaknesses discussed in the "State Approach" section, the most prominent of which is the fact that an automated tool is being trusted to understand database context and generate appropriate migrations;

#### 2.2.2.2 RedGate Source Control (RGSC)

*"SQL Source Control is an add-in for [SQL Server] Management Studio that links your database to your source control system, so you can collaborate on database development with your team."* - from RedGate's official documentation, [41]

RGSC is RedGate's alternative to SSDT. It also works on the basis of an SQL Server Database Project created in Visual Studio, similarly to SSDT (a slightly modified version). However, the workflow then transitions to using its integrated features in SQL Server Management Studio (SSMS).

RedGate provides specific add-ons for CI/CD tools (e.g. Azure DevOps, TeamCity, Octopus Deploy, and Jenkins) to facilitate integration, but also provides Powershell cmdlets to allow integration with any other CI/CD tools, in a more personalized manner.

Compared to SSDT, the author felt that RGSC was more user-friendly and presented a cleaner workflow. Globally, RedGate software is mentioned in several third-party articles and opinion pieces in a generally favourable light. Locally, members of the Porto DevOps community also had positive experiences with RedGate software. Subjective opinions aside, its main benefit is the ability to use custom, human-written migration scripts [44] for those exceptional cases where the script-generator failed to produce an adequate migration. In addition to this, RedGate's documentation and support seem very comprehensive, with several reference manuals, tutorials, actively maintained and moderated forums, and free step-by-step video courses (Author's note: given the author's prior inexperience with databases and CI/CD, all of this documentation was incredibly in getting comfortable not only with RedGate's tools, but also the broader theoretical concepts that are common to other tools)

However, RGSC presents a significant financial cost. After contacting sales representatives to negotiate a quote, the best deal available was approximately €16000 for a 3-year upgrade and support plan for 5 users.

Moreover, it presents the same weaknesses of State approaches in general.


#### 2.2.2.3 ApexSQL

*"ApexSQL Source Control is a Microsoft SQL Server Management Studio add-in, which [sic] integrate source control with SQL Server database development. Before [sic] download and install the add-in, there are some hardware and software minimal requirements that have to be met."* - from ApexSQL Source Control's documentation, [45]

ApexSQL is a suite of independent tools for SQL Server development and administration, with ApexSQL Source Control being the most relevant one for the purposes of this report.

As the above excerpt demonstrates, the documentation, despite its extensiveness, has a fair number of grammatical errors, which is not a good omen for such a critical tool. Combined with the absence of unique selling points, not much of a reputation amongst the DBA/DevOps community (neither good nor bad, simply inexistent) and some issues encountered while installing and setting up the trial version, this tool was swiftly discarded from further consideration.

#### 2.2.2.4 dbForge

dbForge, in a similar manner to RedGate and ApexSQL, provides several plug-ins for SSMS to source control, integrate, and deploy database changes. However, it also provides a unique IDE, "dbForge Studio" [47], which includes the functionalities of all available plug-ins.

dbForge Studio was found to be reliable and better than SSDT, but not particularly distinctive. dbForge's main weakness, however, is its poor documentation, especially when compared with RedGate's.


#### 2.2.2.5 DBMaestro

*"Automate & govern database releases to accelerate time-to-market while preventing downtime & data-loss."*

*"DBmaestro's DevOps Platform: DevOps for the Database"* - from DBMaestro's official website [48]

Unlike other tools in this section, which include an excerpt from their respective documentation, you may have noticed that this section's quote mentions "website" instead of "documentation". That is because **DBMaestro has no documentation to speak of**. It does, however, have plenty of white-papers and case-studies written in with obvious marketing undertones. Since no free trials are available either to try their product first-hand, this option was swiftly discarded (although it is possible to watch an online demo, or book one on-premises).


#### 2.2.2.6 RedGate Change Automation (RGCA)

*"Extend DevOps processes to your SQL Server databases. Develop, source control, and safely automate deployments of database changes with SQL Change Automation."* - from RGCA's documentation, [49]

RGCA is RedGate's product for the Migrations-based approach - RGSC, discussed earlier, is their alternative for the State-based approach.

Similarly to RGSC, it is based on SQL Server Database Projects, integrates into Visual Studio, and provides both specific add-ons for third-party CI/CD tools, as well as more adaptable Powershell cmdlets.

Just like for RGSC, RGCA's documentation, support and userbase are very strong suits.
As well as benefitting from the strong suits of the Migrations approach, RGCA has a particular feature to detect Schema Drift and suggest ways to correct it (although the actual fix has to be performed manually) [25]. Additionally, it provides an "Offline Schema" feature that generates a read-only set of declarative SQL scripts for every object in the development database, emulating the State approach in order to allow developers to consult the state of the database through the scripts, whilst still working according to the Migrations approach.


#### 2.2.2.7 Flyway

*"Flyway is an open-source database migration tool. It strongly favors simplicity and convention over configuration. It is based around just 7 basic commands: Migrate, Clean, Info, Validate, Undo, Baseline and Repair. Migrations can be written in SQL (database-specific syntax (such as PL/SQL, T-SQL, ...) is supported) or Java (for advanced data transformations or dealing with LOBs)."* - from the official Flyway documentation [50].

Flyway is a Java-based tool, simpler than RGCA and with less features. However, its simplicity, lack of dependencies (apart from the Java run-time environment), easy to understand working principle, and the fact that it is free, make it a popular low-cost alternative.

Since migrations can be written in plain SQL, there would not be any further overhead with training or adaption periods from the SQL developers' part.

As described in section 2.1.3.2 "Migrations Approach", Flyway works by creating a meta-table in each environment's database that keeps track of which migrations have been applied, to determine which ones to apply in each deployment. This logic could potentially be built from scratch in the form of an in-house tool that 3C could reasonably develop if it is ever so inclined. This means that using Flyway will result in almost no vendor lock-in (i.e. no proprietary, "black box" technology that would force 3C to rely on a specific vendor or provider, as would be the case with RedGate's tools).

Flyway's biggest weak point is the lack of support. There is a paid version, but if the company would be willing to invest financially in a tool, RedGate is clearly the superior option - not only in terms of features but the available tech support specialists. In addition, its documentation is somewhat plain and basic. Although the commands are mostly self-explanatory, the documentation does not have detailed or advanced use-cases or examples.


#### 2.2.2.8 Liquibase

Liquibase is an open-source tool that makes use of logfiles written in XML, YAML or JSON [51]. It is not possible to write migrations in SQL, which is its biggest weakness. 3C's DBA team showed an immediate disapproval of this issue, so Liquibase was not considered further.


#### 2.2.2.9 Datical

Datical is the paid version of Liquibase. Datical has no publically available documentation [52], no free trials, and no details on how exactly it works (it can be assumed that it is based on XML migrations like Liquibase, but there is no official information on this). Due to the lack of documentation and resources, Datical was not considered further.



### 2.2.3 Continuous Integration and Continuous Delivery tools

This section covers some prominent Continuous Integration and Continuous Delivery (CI/CD) tools on the market. This is a much more fertile and mature market in comparison to the DLM one, with plenty of long-time mainstays (e.g. TeamCity, Jenkins, TravisCI, Azure/Microsoft), as well as competitors that are simultaneously more recent but also widely recognized (e.g. CircleCI, GitLab).

Due to time constraints, only eight tools have been comprehensively studied. After consulting with the relevant stakeholders, the following key points emerged:

 1. 3C prioritizes a cost-effective solution;
 2. 3C intends to use CI/CD on its own infrastructure in the foreseeable short-term, but is aware that moving to cloud-based solutions might be a possibility in the long-term;
 3. 3C is not concerned with IaC-compliance per se, but the intern believes that to be an important criteria, so it should be considered;
 4. 3C would like to avoid using multiple tools (e.g. one for CI, another for CD, and another for issue tracking, and so on), if feasible;

Based on the key topics, the following major questions can be asked when evaluating a tool:

 1. How is it licensed? Is it free? If not, is it cost-effective?
 2. Does it support both a cloud-based service and an on-premises version?
 3. Does it provide features to create and administer both a CI pipeline AND a CD pipeline?
 4. Does it allow building a pipeline as code, in order to comply with the wider Infrastructure as Code (IaC) philosophy?
 5. How does it integrate with other 3rd-party tools? Is there a wide range of plug-ins and integrations, community-based or not?
 6. Does it include the necessary tools to manage projects according to the Agile workflow (e.g. issue tracking, Scrum/Kanban boards, Sprint Planning/Review/Retrospective)?

 Figure 2.22 provides a summary of answers to these questions:

![CI CD tools comparison](CI_CD_comparison_table.png)

Figure 2.22 - CI/CD tools comparison

Perhaps due to the fact that all of these tools are somewhat established, their documentation is very comprehensive and cohesive without exception, so that criterium was not included in the table for brevity's sake. Their IaC capability was included, however, due to the fact that during the internship, some tools were not fully IaC-capable (e.g. Azure DevOps did not allow configuring CD pipelines through code, only through a GUI editor). This illustrates the rapid development and change associated to these tools and the effort their developers make to stay on the cutting edge.

The next sub-sections provide some further important details on each tool, to better understand the comparison table.

### 2.2.3.1 Azure DevOps

Azure DevOps, known as "Team Foundation Server" (TFS) for previous versions, is a Microsoft suite to develop software projects according to the DevOps and Agile methodologies. Its main features are:

 . version control & repository hosting (Azure Repos);

 . Build Artifact package management (Azure Artifacts);

 . CI/CD tools (Azure Pipelines);

 . issue tracking & Agile planning (Azure Boards);

Hosting-wise, Azure DevOps is available as a cloud service (Azure DevOps Service) or as an on-premises server (Azure DevOps Server). Build and Release agents can be a combination of self-hosted and cloud-hosted ones.

Azure DevOps' main benefits are:

 1. Already in use by 3C, in the form of TFS. Although there will most likely be a cost for upgrading to the newer Azure DevOps, the fact that TFS is in use and 3C possesses several Visual Studio subscriptions, the cost will probably be much lower than negotiating a brand new contract with another provider;

 2. Being compliant with Pipelines-As-Code. Both CI and CD pipelines can be declared through YAML files, with code-completion help for common tasks and declarations;

 3. Including repositories and project management features (Azure Repos & Boards). Most other tools are strictly for CI/CD, and other software would be needed to host repositories (unless they would only exist on-premises) or conduct Agile project management (e.g. JIRA, Trello). This benefit streamlines the user-experience for 3C's developers and potentially saves costs;

Azure DevOps' main drawbacks are:

 1. Risk of over-reliance on a single provider. Although it is convenient to access repositories, issues, Sprint Planning, CI/CD reports, etc. from a single platform, it also promotes high-coupling between 3C and Microsoft (as opposed to, for example, having repositories on GitHub, using Trello for Agile planning, and Jenkins for CI/CD definitions);

 2. Confusing UI, at times. Some options and features were not as intuitive and immediate to grasp, when compared to other tools;

 3. Somewhat average breadth of plug-ins and 3rd-party integrations, when compared to the other reviewed tools. Although boasting a healthy marketplace, competitors such as TeamCity and Jenkins seem to have a more massive userbase and community of 3rd-party contributors;


### 2.2.3.2 TeamCity

TeamCity is a Continuous Integration server by JetBrains. First released in 2006, it is one of the most well established options on the market, with an impressive range of 3rd-party plug-ins and integrations.

As stated above, TeamCity is strictly a CI server. With that in mind, here are its main benefits:

 1. Beautiful and intuitive UI. The author was able to attest to its reputation of having a tight and solid UI;

 2. Very strong Pipeline-As-Code capabilities. Beyond using declarative files such as YAML or JSON, TeamCity makes full use of Kotlin to define and configure CI pipelines. Due to Kotlin being a full DSL, this provides much more powerful capabilities than using a purely declarative standard like YAML;

 3. Demonstrated reliability in CI. TeamCity's focus on CI and long history make it a proven and versatile solution for CI;

 4. Good 3rd-party integration;

TeamCity's main drawbacks are:

 1. Not-as-strong CD features. Although it possesses features to deploy/deliver/release, they are not as comprehensive, documented and flexible as is the case for competitor tools. As complexity increases and further needs arise when upgrading 3C's infrastructure, it will probably be necessary to incur in further costs by investing in another CD-specialized tool;

 2. No cloud-hosting for the CI server. Currently TeamCity needs to be hosted on-premises. Although this is not an issue for the 3C in the short term, it might be in the near future, which would imply additional costs of having to run TeamCity in a cloud provider such as Microsoft Azure or Amazon Web Services;


### 2.2.3.3 Octopus Deploy

Octopus Deploy is a Continuous Delivery/Deployment solution. It boasts an impressively large userbase and specializes on CD only.

Octopus Deploy's main benefits:

 1. Very comprehensive and flexible CD features, perhaps the strongest of the entire set of tools. Octopus allows the application of several deployment patterns (e.g. Blue-Green, Canary) through its available options;


Octopus' main weaknesses are:

 1. Strictly CD. Although an interesting solution for CD pipelines, its lack of CI features means that 3C would have to invest in another tool, and additional costs are a major factor;

 2. Somewhat average 3rd-party plug-ins and integrations. With only 328 listed items on its library page, Octopus' selection is not as widespread as some of the other contenders. It is important to note, however, that there are plenty of resources on how to integrate Octopus with TeamCity and Jenkins, two of its most common "sister tools" (since they are both widely used CI servers);


### 2.2.3.4 CircleCI

CircleCI can be used for both CI and CD pipelines. Despite its wide usage, it is a somewhat average tool with no unique selling point, when considering 3C's needs. Furthermore, its offering of plug-ins and integrations is rather lackluster in comparison to its competitors.


### 2.2.3.5 TravisCI


### 2.2.3.6 Bamboo / Atlassian


### 2.2.3.7 GitLab


### 2.2.3.8 Jenkins

Last, but not least, is Jenkins, without a doubt the most popular and widely used free CI/CD tool.

Jenkins' main benefits are:

 1. It's free. Although most of the other tools reviewed so far have free plans for small projects/teams or offer discounted rates, this would not apply to 3C Payment given its scale of databases and infrastructure. Hence, Jenkins is the only truly free, established option on the market;

 2. Exceptional 3rd-party plug-ins and integrations. Tools such as TeamCity and TravisCI have a great selection of hundreds of plug-ins, but they are dwarfed by Jenkins' thousands of options. Jenkins can integrate with just about anything, and its open-source community is probably the most active in creating new ones as required;

 3. Already in use by 3C TechLab for test automation, which means there is already in-house expertise in working with Jenkins;

The main drawbacks for Jenkins are:

 1. Strictly on-premises. If 3C wishes to migrate their infrastructure in the near future, the transition will not be as smooth. It is worth noting, however, that cloud providers such as Azure and AWS already offer reasonable integration features for Jenkins, and since Jenkins is already free, the cost would not be as significant as it would be with other commercial competitors;

 2. Not as user-friendly as other competitors. Although IaC is a paradigm that the author feels will be the norm, Jenkins implies a more "do-it-yourself" approach that can be daunting to beginners. This is being worked by Jenkins' developers through the introduction of Blue Ocean and, more recently, Jenkins X, which attempt to address this minor issue.



## 2.3. Current context at 3C TechLab

 Having espoused the key concepts and theoretical fundamentals related to the problem in section "2.1", and having provided an insight into the most prominent tools and technologies that are based on those concepts and theories in section "2.2", it is now adequate to review the current reality at 3C TechLab.

 Given that the problem at hand is mostly related to IT infrastructure and not to application design (as is the case with most internships), this section will not cover business logic or domain concepts, but instead focus on the current database and server infrastructure. Figure 2.23 is the current map of databases and server clusters employed by 3C TechLab (this map is used as a reference by the DBA team):

 ![3C Payment Servers & Databases](3CPayment_SQL_Server_Estate_0.3.10.jpg)
