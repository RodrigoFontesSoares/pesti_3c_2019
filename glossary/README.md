# Glossary of terms and abbreviations

 Agile

 Bare-metal (servers) -
 
 CD - an acronym that interchangeably refers to both "Continuous Delivery" and "Continuous Deployment", two similar but decidedly distinct concepts. This report uses it to refers to "Continuous Delivery"

 Continuous Delivery -

 Continuous Deployment -

 Continuous Integration / CI -

 DBA - Database Administration

 Declarative (SQL) script

 DevOps

 Fintech -

 Git

 Infrastructure as Code / IaC - a paradigm advocating that software infrastructure should be treated like application source code as much as possible. In very broad terms, this mainly translates into making software infrastructure as reusable and reproducible as possible through the use of source-controlled, plain text, human-readable configuration files. Environemnt virtualization and/or containerization (as opposed to running software on a server's native environment) are also key related concepts;

 Kanban -

 Migration (SQL) script

 Migrations Approach

 Relational database

 Repository

 Scrum -

 SCS - source control system. An alternative designation for VCS

 SQL

 State Approach

 TeamCity - a Continuous Integration server / tool by JetBrains

 Team Foundation Server / TFS

 TFVC

 VCS

 Visual Source Safe 2005 / VSS 2005
