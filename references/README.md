[1] - http://workingwithdevs.com/delivering-databases-migrations-vs-state/

[2] - https://www.infoq.com/articles/deployment-pipeline-database-changes

[3] - https://www.red-gate.com/products/sql-development/sql-change-automation/approaches

[4] - https://www.infoq.com/articles/deployment-pipeline-database-changes

[5] - https://documentation.red-gate.com/sca3/developing-databases-using-sql-change-automation/generating-scripts-to-capture-database-changes/migration-grouping

[6] - https://www.eduardopiairo.com/2016/09/16/portodata-28-july-2016-delivering-changes-for-applications-and-databases/, slide 20

[7] - Jez Humble, David Farley, "Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation", 2010

[8] - https://www.martinfowler.com/articles/continuousIntegration.html

[9] - http://agilemanifesto.org/

[10] - Richard Bellairs - https://www.perforce.com/blog/what-continuous-integration

[11] - https://docs.microsoft.com/en-us/azure/devops/pipelines/release/artifacts?view=azure-devops

[12] - https://continuousdelivery.com/, Jez Humble's website. Co-author of "Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation", 2010, teacher at UC Berkley.

[13] - http://www.informit.com/articles/article.aspx?p=1621865&seqNum=2. Chapter "Anatomy of the Deployment Pipeline" from "Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation", 2010, Jez Humble and David Farley

[14] - http://timothyfitz.com/2009/02/08/continuous-deployment/. The original coinage of the term "Continuous Deployment"

[15] - https://tsqlt.org/ - The SQL testing framework

[16] - https://insights.stackoverflow.com/survey/2018

[17] - https://git-scm.com/ - Git homepage

[18] - https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different - Perforce opinion on the Git vs Mercurial debate

[19] - https://www.perforce.com/blog/vcs/git-vs-svn-what-difference

[20] - Original internship proposal, written by 3C TechLab, attached to this report

[21] - https://support.microsoft.com/en-us/lifecycle/search?sort=PN&alpha=sourcesafe&Filter=FilterNO

[22] - https://docs.microsoft.com/en-us/sql/relational-databases/database-lifecycle-management?view=sql-server-2017 - Database Lifecycle Management, by Microsoft

[23] - https://www.red-gate.com/simple-talk/sql/database-delivery/what-is-database-lifecycle-management-dlm/ - Database Lifecycle Management, by RedGate

[24] - https://www.oracle.com/technetwork/oem/lifecycle-mgmt-495331.html - Database Lifecycle Management, by Oracle

[25] - https://documentation.red-gate.com/sca3/automating-database-changes/automated-deployments/handling-schema-drift - What is Schema Drift, from RedGate's docs

[26] - https://www.atlassian.com/git/tutorials/what-is-version-control - Atlassian's definition of Version Control

[27] - https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control - Git's definition of Version Control

[28] - https://www.atlassian.com/blog/software-teams/version-control-centralized-dvcs - Atlassian's take on DVCS vs CVCS

[29] - https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows - Git on Distributed Workflows

[30] - https://www.perforce.com/blog/vcs/git-vs-svn-what-difference - Perforce on Git vs SVN

[31] - https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History

[32] - https://www.mercurial-scm.org/

[33] - https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different - Perforce on Git vs Mercurial

[34] - https://www.atlassian.com/blog/software-teams/mercurial-vs-git-why-mercurial - Atlassian on Git vs Mercurial

[35] - https://subversion.apache.org/

[36] - https://subversion.apache.org/docs/release-notes/release-history.html

[37] - https://docs.microsoft.com/en-us/azure/devops/repos/tfvc/comparison-git-tfvc?view=azure-devops - Microsoft on Git vs TFVC

[38] - https://docs.microsoft.com/en-us/azure/devops/repos/tfvc/overview?view=azure-devops - Microsoft on TFVC

[39] - https://docs.microsoft.com/en-us/sql/ssdt/sql-server-data-tools?view=sql-server-2017https://visualstudio.microsoft.com/vs/features/ssdt/

[40] - https://docs.microsoft.com/en-us/sql/tools/sqlpackage?view=sql-server-2017

[41] - https://documentation.red-gate.com/display/soc7/

[42] - https://documentation.red-gate.com/sca3/developing-databases-using-sql-change-automation/advanced-project-manipulation/re-baselining

[43] - https://documentation.red-gate.com/display/SCA3/Add-ons - RedGate add-ons

[44] - https://documentation.red-gate.com/display/SOC7/Working+with+migration+scripts

[45] - https://knowledgebase.apexsql.com/apexsql-source-control-first-time-user/

[46] - https://martinfowler.com/books/refactoringDatabases.html

[47] - https://www.devart.com/dbforge/sql/studio/

[48] - https://www.dbmaestro.com/

[49] - https://documentation.red-gate.com/sca3

[50] - https://flywaydb.org/documentation/

[51] - https://www.liquibase.org/index.html

[52] - https://www.datical.com/resources/ - No Datical documentation - only white papers, webinars, videos, and other promotional materials


## Upgrading history from VSS to TFS

The safest bet seems to be a multi-step approach:

1. Migrate from VSS 2005 to TFS 2013 (yes, this specific version of TFS)
https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2013/ms253060(v=vs.120)

https://marketplace.visualstudio.com/items?itemName=EdH-MSFT.VisualSourceSafeUpgradeToolforTeamFoundationServer

2. Migrate from TFS 2013 to DevOps Server 2019 through other intermediary versions of TFS
https://docs.microsoft.com/en-us/azure/devops/server/upgrade/get-started?view=azure-devops

**WARNING:** Apparently migrating "too much" data at a time from VSS to TFS can unexpectedly fail:
https://stackoverflow.com/questions/52199599/migrating-from-vss-to-git-while-keeping-the-history

The author of the issue figured it out himself.

## Migrating commit history from TFVC to Git
https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2013/ms181368%28v%3dvs.120%29

https://simpleprogrammer.com/git-vs-tfvc/

It is possible to migrate the complete commit history on a TFVC repo to a Git one, according to this:

https://www.scrum-tips.com/2018/08/28/migrate-from-tfvc-to-git/

To migrate the FULL history, we need the Git-TFS tool, available on GitHub:
https://github.com/git-tfs/git-tfs/blob/master/doc/usecases/migrate_tfs_to_git.md


## Testing frameworks

https://tsqlt.org/

## Tutorials and Walkthroughs

SQL Source Control:

RedGate + Azure
https://www.red-gate.com/hub/product-learning/sql-change-automation/database-continuous-integration-with-the-redgate-sql-toolbelt-and-azure-devops


RedGate + Git + Jenkins
https://www.red-gate.com/products/sql-development/sql-change-automation/resources/extending-devops-practices-sql-server-databases-redgate-git-jenkins-2

RedGate SQL Change Automation Course (State)
https://www.red-gate.com/hub/university/sql-change-automation-with-sql-source-control-projects

RedGate SQL Change Automation Course (Migration)
https://www.red-gate.com/hub/university/sql-change-automation-with-migrations

http://www.releasemanagement.org/2016/02/database-versioning-using-liquibase/

https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/agents?view=azure-devops

https://blog.codinghorror.com/get-your-database-under-version-control/

https://odetocode.com/blogs/scott/archive/2008/01/31/versioning-databases-the-baseline.aspx

https://medium.com/devopslinks/devops-for-database-4e442abfd939

https://jpvelasco.com/deploying-a-sql-server-database-onto-an-on-prem-server-using-azure-devops/


Docker:

https://github.com/twright-msft/mssql-node-docker-demo-app
