# 1. Introduction

 This project was conducted  

## 1.1. Setting / Context

*"3C TechLab is a new fintech organisation which acts as an innovation incubator developing custom technology for both payment and non-payment solutions. Based in Porto, 3C TechLab is part of the 3C Payment Group, one of the largest Payment Service Providers Worldwide, with offices in Luxembourg, London, Chicago, Dubai, Düsseldorf and Dublin, and customers in 33 countries.  3C TechLab provides technical services, complimentary value add products, and increases the development capacity and speed to market for 3C Payment products."* - From the original internship proposal written by 3C TechLab, the host organization [20].

3C TechLab, created by 3C Payment in 2017 as an innovation hub, develops applications and solutions for its parent company. These applications and solutions work with data stored in 3C Payment's databases, and as these applications and solutions change and evolve over time, so must the databases change accordingly (e.g. when a new feature is being developed from scratch, it is very likely that not only will an application's source code have to be modified, but also the supporting database with, for example, a new stored-procedure that fetches specific data). When a database needs to be upgraded to support a new application feature, 3C TechLab's DBA team has to work on the changes and manually prepare a release version of that database's new version. This **release process** is time-consuming, error-prone, stressful, and could be spent solving other meaningful issues.

In the past 10-15 years, the software industry has made great strides in automating the release process for applications by using industry-standard practices such as Continuous Integration and Continuous Delivery (CI/CD). 3C TechLab's DBA team recognizes the benefits that this automation brings - faster delivery of new features, less bugs, less maintenance, more availability to solve other complex issues, and ultimately more satisfied customers - but due to time and manpower constraints, it has not been able to properly research and test a solution that applies CI/CD to databases (which, unlike applications, have not been subjected to the same kind of progress in release automation).

Given ISEP's proven track record of training its Software Engineering students in state-of-the-art technologies, a curricular internship is a cost-effective way to attempt to solve this problem. An intern will have to design and implement a solution, and for the chosen student/intern, it will be a unique opportunity to gain knowledge and experience in the DBA/DevOps field.

*Author's personal note on motivation: ISEP employs a lot of effort to give us a wide range of insights in different domains/job roles in the IT industry. However, most of its focus is in application development - front-end, back-end and full-stack - making other IT fields/job roles look like an afterthought (e.g. DBA, SysAdmin, QA, Business Analysis, etc). This is understandable, because it is extremely difficult to train students in classical, theoretical computer-science and software fundamentals, while at the same time training them in modern technologies, practices and frameworks so they can be competitive in the job market right out of higher-education.

With all that in mind, the main reason I took on this internship is the fact that everything is brand new: from having to deal with databases in more detail, to building CI/CD pipelines, and researching these domains extensively.*

## 1.2. Problem Statement

*"The main goal of this internship is the migration from our database source repository from Visual Source Safe to Team Foundation Server, and create a mechanism for Continues integration/deploy of our databases."* - From the original internship proposal written by 3C TechLab, the host organization [20].

As part of its core business, 3C Payment maintains several on-premises relational databases of critical customer data, implemented in Microsoft's SQL Server. In order to achieve shorter delivery times on new database features and bug fixes, and shift its DBA team away from menial tasks related to database releases and maintenance, 3C TechLab wishes to apply the industry-standard practices of Continuous Integration (CI) and Continuous Delivery (CD) to its database development process. However, given that these practices are usually applied to stateless software applications (e.g. desktop apps, web pages, mobile apps), certain challenges arise when trying to apply CI/CD to databases, such as:

 . how can data be **preserved** before and after each new deployment;
 . how can database deployments be kept **fast**, given the massive amount of stored data;
 . how can deployments to customer-facing environments be performed **safely**, given that errors related to customer data can have a devastating, long-term impact on the business.

Given these issues, traditional CI/CD tools typically applied to application development will not suffice by themselves. This is not only an issue of CI/CD, but also of "Database Lifecycle Management" (DLM). Several DLM tools exist on the market, both commercial and open-source, and they will have to be integrated with the chosen CI/CD tools/servers.

But before these database CI/CD challenges can be addressed, there is a secondary issue to tackle: the outdated Version Control System (VCS) currently in use to source control SQL scripts. Each of 3C's databases is described by a set of SQL scripts, which are in turn source controlled through "Visual Source Safe 2005", a centralized VCS, hereafter referred to as "VSS 2005". Its last ever version was released in January 2006, and is no longer supported in any capacity by Microsoft as of July 11th, 2017 [21]. Considering that most CI/CD tools on the market have no mention of supporting/integrating with such an outdated VCS, it is imperative to first migrate the existing repository of database SQL scripts to a more modern VCS (e.g. TFVC or SVN for centralized VCS; Git or Mercurial for distributed VCS), whilst **preserving the existing history of changes** as much as possible for auditability purposes.

Working in close proximity to the DBA / data platform team, this internship is therefore focused on solving two distinct, but related issues:

 1. Migrate VSS 2005 repositories of SQL scripts to a modern VCS;
 2. Design and implement a system that applies CI/CD practices and processes to 3C TechLab's databases.

### 1.2.1. Goals, Objectives and Expected Outcomes

*"The work involves the following steps, which must be carried out continuously in an agile and recursive approach:
• Analysis of our current database source control system in Visual Source Safe.
• Generate a plan to migrate from Visual Source Safe to Team Foundation Server, keeping all the history from VSS.
• Implement and deploy that plan.
• Design and develop a solution in order to deploy the databases in the development laptops."* - From the original internship proposal written by 3C TechLab, the host organization [20].

It can be inferred from this excerpt that this internship considers two major goals, from 3C TechLab's business perspective:

. Goal 1 - Migrate VSS 2005 repositories of SQL scripts to a modern VCS;

. Goal 2 - Design and implement a system that applies CI/CD to 3C TechLab's databases.

These goals might be concise and appropriate for an overview of the internship, but further target details are in order. Consider that, due to time and security constraints, only one of 3C TechLab's databases (henceforth "SampleDB") can be included in the scope of this internship. Here are some key targets to consider for each of the defined goals:

 . Goal 1 - Migrate from VSS 2005

  - Ideal success target: SampleDB's source control repository has been migrated from VSS 2005 to a modern VCS, with its **entire history preserved**, particularly in terms of authors, dates and times of changes;

  - Optimal success target: SampleDB's source control repository has been migrated from VSS 2005 to a modern VCS, with **most of its history preserved**. Although somewhat subjective, this scenario contemplates the likelihood of the impossibility of migrating all of VSS 2005's version control items. Available VCS migration tools make no guarantee of being able to migrate every single aspect of a VCS to a different one (e.g. Git to TFVC, SVN to Git), making this scenario a more realistic, yet still achievable one;

  - Minimum success target: SampleDB's source control repository has been migrated from VSS 2005 to a modern VCS, with **none of its history preserved**, but with all previously existing files transferring over to the new VCS;

  - Failure condition: SampleDB's source control repository has not been migrated from VSS 2005 to a modern VCS, not even source files;

. Goal 2 - Implement CI/CD

  - Ideal success target: SampleDB has been integrated into a complete CI/CD pipeline prototype. 3C's stakeholders **approve the prototype**, and decide to use it as the basis for implementing CI/CD to 3C's live, Production databases;

  - Optimal success target: SampleDB has been integrated into a complete CI/CD pipeline prototype. 3C's stakeholders **reject the prototype**, but recognize its value (and by extension the value of the intern's work) and that further prototyping is a viable way to keep working on this issue. This is a more realistic scenario than the "Ideal" one, since there might be a number of unforeseen reasons for the stakeholders to reject the prototype that do not detract from the intern's work;

  - Minimum success target: SampleDB has been integrated into a complete CI/CD pipeline prototype. 3C's stakeholders **reject the prototype**, and believe that despite the intern's best efforts, the work developed so far has not added value to the company;

  - Failure condition: A functioning prototype has not been build, OR 3C's stakeholders believe that the intern has not demonstrated enough knowledge of the problem and subject matter to warrant further work.

Beyond the above targets, which relate to 3C TechLab's business interests, there are equally important outcomes that relate to the intern:

 1. Gain knowledge and experience in Version Control Systems, Database Lifecycle Management, and Continuous Integration & Continuous Delivery tools;

 2. Gain practical experience in researching and cataloguing new concepts and technologies, and systematize that information clearly and concisely, to be used as future reference by other developers;

 3. Gain valuable real-life experience in presenting and pitching innovative ideas to decision makers, and understanding what it takes to get new projects and initiatives financed and approved.

### 1.2.2. Expected Approach

 Given the prior Goals and respective targets, here is an a priori overview of the work to be developed, divided by Goal:

   . Goal 1 - Migrate from VSS 2005

     1. Analyze modern VCS options (e.g. Git, Mercurial, SVN, TFVC) and understand their purpose, advantages, strong suits and weak points;
     2. Decide on a VCS to migrate the database source-control repositories to, and justify that decision based on the previous analysis and also on how they potentially integrate with possible DLM and CI/CD tools used for Goal 2 (e.g. it will be a waste of time to declare Git as the best alternative at this stage, only to later realize that no available CI/CD tool integrates with Git. Most tools do integrate with Git, but this is merely an example);
     3. Analyze available tools to handle VCS migration to the VCS chosen in the previous topic, including possible limitations, and select one to be applied during the internship;
     4. Migrate SampleDB's repository to the new VCS;

  . Goal 2 - Implement CI/CD on a sample 3C database

     1. Analyze existing CI/CD tools (e.g. Jenkins, Azure DevOps, TeamCity) and understand how they work, their purpose, advantages, strong suits, weak points and pricing/licensing;
     2. Select the most appropriate two to three tools for further consideration;
     3. Analyze existing DLM tools (e.g. Microsoft SSDT, RedGate SQL Change Automation, Flyway, ApexSQL) and understand how they work, their purpose, advantages, strong suits, weak points and pricing/licensing;
     4. Choose the three to four most appropriate DLM tools;
     5. Create proof-of-concepts (POC) based on the selected CI/CD and DLM tools. Each POC will integrate one CI/CD tool and one DLM tool into a CI/CD system, applied to a sample database. Use each POC to gain further practical understanding of the DLM and CI/CD tools, anticipating potential issues that each of them might have;
     6. Decide on a pair of DLM and CI/CD tools, and discuss that option with the DBA team. The intern and the DBA might choose the given option, or decide on a different one based on their discussion;
     7. If the DBA team approves, and grants the required permissions, implement the CI/CD system decided on the previous discussion, using a sample database and available on-premises servers;
     8. If the DBA team is comfortable with adding a live, customer-facing Production Environment to the CI/CD system, assist with its implementation.

Figure 1.1 provides an overview of the available tools:

![VCS, DLM and CI/CD examples](INTRO_Approach.png)

Figure 1.1 - examples of VCS, DLM and CI/CD tools

### 1.2.3. Contributions

### 1.2.4. Work Planning

The internship took place over a 90-day period, roughly speaking (01/04/2019 - 30/06/2019). Figure 1.2 shows a projected task list for the whole internship, and Figure 1.3 shows the respective Gantt chart:

![Internship Tasks](Gantt_Chart_Tasks.png)

Figure 1.2 - Expected task list

![Internship Tasks Chart](Gantt_Chart.png)

Figure 1.3 - Expected Gantt chart

For simplicity's sake, most tasks have been projected for either 1 or 2 weeks time. The internship has been divided into four major tasks:

 1. "Study Existing Tools on the Market" - Given the intern's unfamiliarity with the finer details of VCS, DLM and CI/CD tools, and the impending responsibility of choosing/suggesting which tools to use, it is fundamental to gain a deeper understanding of each of these domains. Therefore, a 2-month period has been assigned to study as many tools as possible, compile and process all collected information, build test prototypes, and present findings to the DBA team, in order to decide which tools to use;

 2. "Migrate 1 sample 3C Database ("SampleDB") from VSS 2005 to Git" - after the intern and the DBA team have agreed on which tools to use (including the VCS), it is time to migrate the source-controlled SQL-scripts of a sample database from VSS 2005 to a more modern VCS. Git is expected to be the adopted VCS, but this decision is subject to change. The migration itself should occur without any significant issues, so a 2-week period should suffice;

 3. "Build CI/CD pipeline prototype based on "SampleDB", to present to 3C CTO - assuming that the DBA team is satisfied with the intern's work thus far, and have faith in his knowledge of the problem domain, at this time it will be necessary to convince 3C's CTO to invest in the chosen VCS, DLM and CI/CD tools. Therefore, a demo/prototype should be designed and implemented to be part of a pitch to 3C's CTO. This prototype will be based on one of those built during "1.", so a 10-11 day period is expected;

 4. "Prepare presentation and pitch for 3C's CTO for his final verdict" - with the demo/prototype ready, the final stage is to prepare a pitch and supporting presentation. Practice makes perfect, and once the pitch is ready (presentation + demo), a trip to 3C's headquarters in London will be in order to meet with the CTO. This stage takes place during the final week and a half of the internship, wrapping it up.

## 1.3. Report Structure

 This report is structured into five main sections: Introduction, State Of The Art, Analysis and Design, Implementation and Conclusions:

 1. Introduction - provides some context for the internship, presents the problem to be solved, highlights select contributions that made this work possible, and shows the planning of the work to be done;

 2. State Of The Art - introduces some theoretical fundamentals that help in understanding the problem, its domain, and key concepts and approaches. Expounds on similar works by other authors and practitioners on the subject of VCS, DLM and CI/CD, as well as existing tools and technologies that are commonly used to solve similar problems. Provides insight into the strengths, weaknesses, and suitability of each of these technologies;

 3. Analysis and Design - states which practices, approaches, tools and technologies are to be used to solve the problem, justifying their usage and why one might be preferable to another, expanding upon the analysis conducted in the "State Of The Art". Describes a proposed solution to the problem as abstractly as possible;

 4. Implementation - details all of the technical work conducted in order to bring to fruition the solution that is proposed in "Analysis and Design";

 5. Conclusions - critiques the implemented solution as it pertains to its strong suits, if and how it solves the problem, and what needs to be improved in the future.
